# MATLAB

![MATLAB logo](./images/matlablogo.png)

[MATLAB](https://www.mathworks.com/products/matlab.html) is a
high-performance language for technical computing. It integrates
computation, visualization, and programming in an easy-to-use
environment where problems and solutions are expressed in familiar
mathematical notation. MATLAB features a family of add-on
application-specific solutions called toolboxes. Toolboxes are
comprehensive collections of MATLAB functions (M-files) that extend
the MATLAB environment to solve particular classes of problems. These
are the toolboxes installed in NERSC MATLAB, along with the number of
licenses.

* Deep Learning (1)
* Image Processing (2)
* Optimization (2)
* Parallel Computing (4)
* Signal Processing (1)
* Statistics and Machine Learning (4)
* Compiler (1)

The name MATLAB stands for matrix laboratory. MATLAB was originally
written to provide easy access to matrix software developed by the
LINPACK and EISPACK projects. Today, MATLAB engines incorporate the
LAPACK and BLAS libraries, embedding the state of the art in software
for matrix computation.

## How to Use MATLAB on Cori

MATLAB is available at NERSC on Cori.  The number of MATLAB
licenses at NERSC is not very large (currently 16), so users should
not be running a MATLAB session when it is not being actively used. If
you use NX, it's particularly easy to think you've released your
license when you haven't, since license checkouts persist between NX
sessions.

MATLAB can be run interactively or in batch mode.  MATLAB can run on a
compute node with exclusive access to the full usable memory (about
120 GB on Cori Haswell nodes) by submitting a batch job to the regular
queue.

### Modules

To run MATLAB at NERSC, you will need to load the module file for the 
version of MATLAB you wish to use.

```
module load matlab/R2020b
matlab
```

!!! warning
    MATLAB R2020b loads its own libcrypto.so, so when you finish using MATLAB, 
    you may need to unload the module in order to use salloc, SSH, or SCP. To
    avoid issues, it's best to avoid loading the R2020b module in dot files.

### Running Interactively

To run MATLAB interactively on Cori, connect with ssh -Y or via NX,
and then do the following:

```
salloc  -q interactive -N 1 -c 32 -C haswell -t 30:00
module load matlab
matlab
```

It is also possible to run MATLAB on a Cori login node
directly. Production computing should not be undertaken on login
nodes, however. 

!!! warning
    For long-running or compute intensive jobs, use a batch
    script.

```
module load matlab
matlab
```

### Batch Jobs

To run one instance of MATLAB non-interactively through a batch job,
you can use the following job script on Cori:

```slurm
#!/bin/bash
#SBATCH -q regular
#SBATCH -N 1
#SBATCH -c 32
#SBATCH -C haswell
#SBATCH -t 00:30:00
cd $SLURM_SUBMIT_DIR   # optional, since this is the default behavior
module load matlab
srun -n 1 -c 32 matlab -nodisplay -r < myjob.m -logfile myjob.log
```

Where `myjob.m` is your MATLAB script.

### Parallelism in MATLAB

With our current licensing arrangement, users can easily run MATLAB on 
a single node with many cores, but running on multiple nodes requires 
running compiled code (see below). To prepare a MATLAB cluster with 32 
cores (within MATLAB) do:

```matlab
cluster = parcluster('local')
cluster.NumWorkers = 32
saveAsProfile(cluster,'cori_cluster')
pp = parpool('cori_cluster', 32)
```

#### Running MATLAB Parallel Commands

The following program illustrates how MATLAB parallel commands can be
used on Cori. NERSC's license currently limits use of parallel commands to a
single node and the number of threads that one node supports.

```matlab
% hello-world.m
pc = parcluster('local');
parpool(pc, 32);

spmd
  rank = labindex;
  fprintf(1,'Hello %d\n',rank);
end
```

For loop-level parallelism, MATLAB provides the parfor construct.

#### Running on many cores

For only a few cores, MATLAB will probably run parallel commands with no trouble. Running
on tens of cores will require many more processes. If you find that MATLAB fails to start
up a parallel pool, try increasing the limit on the number of running processes you may 
initiate.

```
ulimit -u 32000
```

#### Parallelism with the MATLAB Compiler

Another way to run MATLAB in parallel is to run multiple instances of
a compiled MATLAB program. This is the approach needed to run on multiple nodes. 
The MATLAB license server normally treats each instance of the application running 
on a separate node as an additional seat, and since we have per-seat licensing, 
using mulitple seats prevents other users from running jobs. By compiling, you 
create a stand-alone application that doesn't need to obtain a separate license from the
NERSC license server to run. This means that each instance of your compiled MATLAB 
script will need to receive its own data (running "data parallel"). See 
[MATLAB Compiler](matlab_compiler.md) for details.

## Documentation

Extensive [on-line documentation](http://www.mathworks.com/) is
available. You may subscribe to the MATLAB Digest, a monthly e-mail
newsletter by sending e-mail to <subscribe@mathworks.com>.

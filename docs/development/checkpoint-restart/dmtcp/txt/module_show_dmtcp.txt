cori$ module show dmtcp
-------------------------------------------------------------------
/usr/common/software/modulefiles/dmtcp/2019-10-24:
   
module-whatis    DMTCP (Distributed MultiThreaded Checkpointing) transparently checkpoints
                 a single-host or distributed computation in user-space -- with no modifications
                 to user code or to the O/S.
setenv           DMTCP_DIR /global/common/sw/cray/cnl7/haswell/dmtcp/2019-10-24
setenv           DMTCP_DL_PLUGIN 0
prepend-path     LD_LIBRARY_PATH /global/common/sw/cray/cnl7/haswell/dmtcp/2019-10-24/lib
prepend-path     PATH .
prepend-path     PATH /global/common/sw/cray/cnl7/haswell/dmtcp/2019-10-24/bin
prepend-path     MANPATH /global/common/sw/cray/cnl7/haswell/dmtcp/2019-10-24/share/man

-------------------------------------------------------------------


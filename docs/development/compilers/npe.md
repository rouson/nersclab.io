# NPE and PrgEnv-llvm

NPE or NERSC Programming Environment complements and extends the Cray
Programming Environment provided by HPE. 

!!! warning "NPE and PrgEnv-LLVM are currently _Experimental_" 
	These are new efforts under active development. Long 
	term support options are being planned and evaluated, but
	for now significant breaking changes may occur at any time.
	
	Your feedback is welcome!

## Usage

NPE is not yet part of the default set of modules and must be added
via:

```
module use /global/cfs/cdirs/nstaff/cookbg/pe/modulefiles
```

after which NPE can be loaded

```
ml npe
```

!!! note 
	Additional versions of the `npe` stack will be made available as
	additional `npe` modules.

### PrgEnv-llvm

LLVM compilers with support for OpenMP offload and SYCL with support
for MPI via MPICH.

```
ml PrgEnv-llvm
```

If GPU support is desired then

```
ml cudatoolkit craype-accel-nvidia80
ml PrgEnv-llvm
```

!!! note
	Use `mpic++`, `mpicc` to compile, _not_ `cc` or `CC`.
	
!!! note
	Fortran (`flang`) is not supported at this time.
	
`intel-llvm` is the default version and supports OpenMP offload and
SYCL for A100. Nightly versions are also available (see `ml avail
intel-llvm`).

`llvm` is based on llvm.org released versions and supports OpenMP
offload for A100.

### MPICH

The `mpich` module is available for:

* `PrgEnv-gnu`
* `PrgEnv-nvidia`
* `PrgEnv-llvm`

!!! tip 
	`mpic++`, `mpicc` and `mpif90` should be used instead of `cc`,
	`CC`, and `ftn` in this release.

!!! tip CUDA Aware
	The `cudatoolkit` and `craype-accel-nvidia80`
	modules **must** be loaded before `mpich` to enable CUDA aware
	MPI.
	
### flux

[Flux](https://github.com/flux-framework/flux-core) is a flexible
framework for resource management consisting of a suite of projects,
tools, and libraries which may be used to build site-custom resource
managers for HPC centers.

!!! warn
	Reminder that everything in NPE is considered "experimental"
	[NERSC's current workflow tool recommendations](../../jobs/workflow-tools.md).

Start multi-node interactive job

```slurm
salloc -C gpu -A <account>_g -N 2 --ntasks-per-node=1 --gpus-per-node 4 -q debug -t 30
```

Load the `flux-sched` module

```bash
module load flux-sched
```

Start flux

```slurm
srun --mpi=pmi2 --pty flux start
```

Verify that flux can see gpu resources

```console
$ flux resource list
     STATE NNODES   NCORES    NGPUS NODELIST
      free      2      128        8 nid[001704-001705]
 allocated      0        0        0
      down      0        0        0
```

## Support

Please report any issues, comments or feedback via the [NERSC Help
Desk](https://help.nersc.gov).

## Release notes 

### 22.06

* `flux` support added (`flux-sched/0.22.0`)
* Reframe test for flux
* also build `llvm/14.0.0`

### 22.05

Initial "alpha" release includes

* `mpich/4.0` for `PrgEnv-gnu`, `PrgEnv-nvidia` and `PrgEnv-llvm` [mpich source](https://github.com/pmodels/mpich)
* `PrgEnv-llvm/0.1` a new LLVM based `PrgEnv`
* `llvm/14.0.3` from llvm.org [llvm.org source](https://github.com/llvm/llvm-project.git)
  with OpenMP offload support for A100.
* `intel-llvm/2021-12` from [Intel's fork of LLVM](https://github.com/intel/llvm.git)
  with OpenMP offload and SYCL support for A100.
* `intel-llvm/sycl-nightly/<date>` Access to nightly builds of `intel-llvm`.
* `ninja/1.10.2` Ninja build tool
* `cmake/3.22.1` CMake build system
* `reframe/3.10.1` HPC testing framework

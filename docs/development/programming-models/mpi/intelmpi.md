# Intel MPI

[Intel MPICH](https://software.intel.com/content/www/us/en/develop/tools/mpi-library/get-started.html) 
is available and uses the OFI libfabric transport.

## Building for Intel MPI

To build applications against Intel MPI at NERSC, first load the `impi` module:

```shell
module load impi
```

The simplest way to compile your application for Intel MPI is to use the 
Intel MPI compiler wrappers: 

```shell
mpiicc -o my_c_exec my_c_prog.c
mpiicpc -o my_cplusplus_exec my_cplusplus_prog.cxx
mpiifort -o my_f90_exec my_f90_prog.f90
```

Note that these wrappers will use the Intel compilers regardless of which 
`PrgEnv-` module you have loaded. You can pass compiler options through 
the wrappers as if you were invoking the Intel compilers directly.

### Job Launch

The `impi` module must be loaded to run jobs using Intel MPI. After loading the
module you can use `srun` to launch the job:

```shell
salloc -N 5 --ntasks-per-node=32 -C haswell
module load impi
srun -n 160 ./my_c_exec
```

(See also our [running jobs example for Intel MPI](../../../jobs/examples/index.md#intel-mpi)).

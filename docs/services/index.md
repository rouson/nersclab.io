# Services at NERSC

- [bbcp](bbcp.md) - Bbcp is a point-to-point network file copy application with excellent
network transfer rates, this service is available on NERSC Data Transfer Nodes.

- [Cern VM File System (CVMFS)](cvmfs.md) - is a software distribution service to
provide data and software for jobs.

- [CDash](cdash/index.md) - is an open source, web-based software testing server developed
by Kitware. CDash is typically part of a larger software process that integrates
Kitware's CMake, CTest, and CPack tools, as well as other external packages used
to design, manage and maintain large-scale software systems.

- [GitLab](gitlab.md) - is a DevOps platform for software development teams to collaborate
together by hosting code in a source repository. It can also automate building, integrating and verifying
code, a process often called continuous integration and used with continuous development (CI/CD).
You can access GitLab at https://software.nersc.gov.

- [Globus](globus.md) - Recommended tool for moving large amounts of data between NERSC
and other systems.

- [GridFTP](gridftp.md) - Command Line interface for parallel data movement.

- [Jupyter](jupyter.md) - is a web application for creating notebooks containing
 code and useful for data analytics. We support [JupyterHub](https://jupyterhub.readthedocs.io/en/stable/)
 which is a Jupyter server that provides multi-user hub for creating/spawning jupyter
 notebooks. JupyterHub service can be found at [jupyter.nersc.gov](https://jupyter.nersc.gov/)

- [NEWT](newt.md) - NEWT is a [web service](https://newt.nersc.gov/) that allows
you to access computing resources at NERSC through a simple RESTful API.

- [Superfacility API](sfapi/index.md) - is a [web service](https://api.nersc.gov/api/v1.2) that allows
you to access a wide range of NERSC resources (status/transfer/computing/accounting) through a RESTful API.

- [RStudio](rstudio.md) - is an enterprise software for development of R for
statistical computing. The RStudio can be accessed at [rstudio.nersc.gov](https://rstudio.nersc.gov)

- [Science Gateways](science-gateways.md) - A science gateway is a web-based
interface to access HPC computers and storage systems.

- [scp](scp.md) - Secure Copy (SCP) is used securely transfer files between two
hosts via SSH protocol.

- [Spin](spin/index.md) - is a service platform at NERSC based on docker containers that
 can access NERSC systems, storage, and backend system.

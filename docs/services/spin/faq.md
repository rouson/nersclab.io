# Frequently Asked Questions

By now hopefully you have a good understanding of how to get your app running on Spin. Here we answer some common
questions and give recipes for common application services.

## I noticed strange annotations in my deployment's yaml, for example `nersc.gov/collab_uids`. Where do they come from?

Spin has some NERSC-specific components to enforce security and make sure users don't accidentally access each others'
data. We use Kubernetes [mutating
webhooks](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/) to add annotations
to your application's yaml during deployment. At start time, the [Open Policy
Agent](https://www.openpolicyagent.org/docs/v0.12.2/kubernetes-admission-control/) ensures that the annotations are
present and that the running app is using shared resources (eg. the Community Filesystem) in accordance with the
declared values.

## What are the rules that are enforced by these NERSC-added annotations?

The rules enforced by [OPA](https://www.openpolicyagent.org/docs/v0.12.2/kubernetes-admission-control) contain about 50
checks, ranging from internal consistency (is the annotation present?) to shared resource use (is the application owner
allowed to access shared storage at path X?).

## I found some code on stackoverflow.com, but it doesn't work in Spin. Why is that?

While Spin is running Kubernetes under the hood, it is customized for our NERSC "cloud". Not all features of Kubernetes
will work in Spin.

## How do I run a Slurm job from Spin?

You have several options for running a job on another NERSC compute resource from Spin:

- You could host an API service in Spin that is called from the Supercomputer when your job is done

- You could save a job script to the Community filesystem from Spin (see ["Storing data in
  Spin"](../storage/#external-volume-slower-but-visible-outside-of-spin)) and have a cronjob (or
  [similar](https://kafka.apache.org/)) running on the login nodes run it

- You could use the [Superfacility API](../../sfapi/) to run a job (see "POST /compute/jobs/{machine}" in our [swagger
  docs](https://api.nersc.gov/api/v1.2/))

## Can I deploy my Spin app using [Helm Charts](https://helm.sh/docs/topics/charts/)?

Yes! Please see our [documentation about using Helm](../helm).

## I'm stuck! How do I get help?

Please see the "Get Started and Get Help" section of the [Spin documentation](https://www.nersc.gov/systems/spin/). You
can also get more information by attending a [SpinUp Workshop](../#spinup-workshops).
